// *  Сетит поле в переданный сеттер

const commonFieldUpdate = (field, value, setter) => {
	setter((prev) => {
		return { ...prev, [field]: value }
	})
}
export { commonFieldUpdate }
