import React, { useState } from 'react'
import { Button, Avatar, Menu, MenuItem, IconButton, Box } from '@mui/material'
import styles from './Header.module.scss'
import Container from '@mui/material/Container'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { logOut, selectIsAuth } from '../../redux/slices/auth'

export const Header = () => {
	const dispatch = useDispatch()
	const isAuth = useSelector(selectIsAuth)
	const userData = useSelector((state) => state.auth.data)

	const [anchorEl, setAnchorEl] = useState(false)

	const onClickLogout = () => {
		if (window.confirm('Вы действительно хотите выйти?')) {
			dispatch(logOut())
			window.localStorage.removeItem('token')
		}
	}
	return (
		<div className={styles.root}>
			<Container maxWidth="lg">
				<div className={styles.inner}>
					<Link className={styles.logo} to="/">
						<div>О РЫБАЛКЕ</div>
					</Link>
					<div className={styles.buttons}>
						{isAuth ? (
							<Box style={{ display: 'flex', alignItems: 'center' }}>
								<Link to="/add-post">
									<Button variant="contained">Написать статью</Button>
								</Link>

								<Box style={{ display: 'flex', alignItems: 'center' }}>
									<IconButton
										size="large"
										aria-label="account of current user"
										aria-controls="menu-appbar"
										aria-haspopup="true"
										onClick={(e) => {
											setAnchorEl(e.currentTarget)
										}}
										color="inherit"
									>
										<Avatar alt={userData.fullName} src={userData.avatarUrl}>
											{userData.fullName.slice(0, 1).toUpperCase()}
										</Avatar>
									</IconButton>
									<div>{userData.fullName}</div>
									<Menu
										id="menu-appbar"
										anchorEl={anchorEl}
										keepMounted
										anchorOrigin={{
											vertical: 'bottom',
											horizontal: 'left',
										}}
										transformOrigin={{
											vertical: 'top',
											horizontal: 'left',
										}}
										open={Boolean(anchorEl)}
										onClose={() => setAnchorEl(null)}
									>
										<MenuItem>
											<Link
											//  to="/posts/create"
											>
												<Button variant="contained">Профиль</Button>
											</Link>
										</MenuItem>
										<MenuItem onClick={() => setAnchorEl(null)}>
											<Button
												style={{ width: '100%' }}
												onClick={onClickLogout}
												variant="contained"
												color="error"
											>
												Выйти
											</Button>
										</MenuItem>
									</Menu>
								</Box>
							</Box>
						) : (
							<>
								<Link to="/login">
									<Button variant="outlined">Войти</Button>
								</Link>
								<Link to="/register">
									<Button variant="contained">Создать аккаунт</Button>
								</Link>
							</>
						)}
					</div>
				</div>
			</Container>
		</div>
	)
}
