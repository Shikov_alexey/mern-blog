import React from 'react'

import styles from './AddComment.module.scss'

import TextField from '@mui/material/TextField'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'

export const Index = ({ onChange, onSendBtnClick, value, avatarUrl }) => {
	return (
		<>
			<div className={styles.root}>
				<Avatar
					classes={{ root: styles.avatar }}
					src={avatarUrl ? `http://localhost:4444${avatarUrl}` : ''}
				/>
				<div className={styles.form}>
					<TextField
						value={value}
						onChange={onChange}
						label="Написать комментарий"
						variant="outlined"
						maxRows={10}
						multiline
						fullWidth
					/>
					<Button onClick={onSendBtnClick} variant="contained">
						Отправить
					</Button>
				</div>
			</div>
		</>
	)
}
