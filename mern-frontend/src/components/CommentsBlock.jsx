import React from 'react'

import { SideBlock } from './SideBlock'
import ListItem from '@mui/material/ListItem'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import ListItemText from '@mui/material/ListItemText'
import Divider from '@mui/material/Divider'
import List from '@mui/material/List'
import Skeleton from '@mui/material/Skeleton'
import moment from 'moment'
import { dateFormat } from '../constants'

export const CommentsBlock = ({ items, children, isLoading = true }) => {
	return (
		<SideBlock title="Комментарии">
			<List>
				{(isLoading ? [...Array(5)] : items).map((e) => {
					const data = {
						...e,
						key: e._id,
						user: {
							...e.postedBy,
						},
					}
					return (
						<React.Fragment key={data.key}>
							<ListItem alignItems="flex-start">
								<ListItemAvatar>
									{isLoading ? (
										<Skeleton variant="circular" width={40} height={40} />
									) : (
										<Avatar alt={data.user.fullName} src={data.user.avatarUrl} />
									)}
								</ListItemAvatar>
								{isLoading ? (
									<div style={{ display: 'flex', flexDirection: 'column' }}>
										<Skeleton variant="text" height={25} width={120} />
										<Skeleton variant="text" height={18} width={230} />
									</div>
								) : (
									<>
										<ListItemText
											style={{ width: '90%' }}
											primary={data.user.fullName}
											secondary={data.text}
										/>
										<ListItemText
											style={{ textAlign: 'end' }}
											secondary={
												moment.isMoment(data.time)
													? moment(data.time, 'YYYY-MM-DD').format(dateFormat)
													: data.time
											}
										/>
									</>
								)}
							</ListItem>
							<Divider variant="inset" component="li" />
						</React.Fragment>
					)
				})}
			</List>
			{children}
		</SideBlock>
	)
}
