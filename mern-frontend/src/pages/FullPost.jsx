import React, { useEffect, useState } from 'react'
import ReactMarkdown from 'react-markdown'
import moment from 'moment'
import { Post } from '../components/Post'
import { Index } from '../components/AddComment'
import { CommentsBlock } from '../components/CommentsBlock'
import { useParams } from 'react-router-dom'
import { axios } from '../utils/axios'
import { dateFormat } from '../constants'
import { commonFieldUpdate } from '../utils/field-updaters'
import { useSelector } from 'react-redux'
import { selectAuthData } from '../redux/slices/auth'

export const FullPost = () => {
	const { id } = useParams()
	const [data, setData] = useState({ isLoading: true })
	const authData = useSelector(selectAuthData)

	useEffect(() => {
		const getData = async () => {
			await axios
				.get(`/posts/${id}`)
				.then((res) => {
					setData({ ...res.data, isLoading: false })
				})
				.catch((err) => console.warn(err))
		}
		getData()
	}, [])
	if (data.isLoading) {
		return <Post isLoading={data.isLoading} isFullPost />
	}

	const onSendBtnClick = async () => {
		if (data.comment?.length && authData._id) {
			const body = {
				comments: [
					...data.comments,
					{ postedBy: authData._id, text: data.comment, addOrUpdateTime: moment() },
				],
			}
			const dataFromServer = await axios
				.patch(`/posts/comments/${id}`, body)
				.catch((err) => console.log(err))
			setData({ ...dataFromServer.data })
			commonFieldUpdate('comment', '', setData)
		}
	}

	return (
		<>
			<Post
				id={data._id}
				key={data._id}
				title={data.title}
				imageUrl={data.imageUrl ? `http://localhost:4444${data.imageUrl}` : ''}
				user={data.user}
				createdAt={moment(data.createdAt).format(dateFormat)}
				viewsCount={data.viewCount}
				commentsCount={data.comments?.length}
				tags={data.tags}
				isLoading={data.isLoading}
				isFullPost
			>
				<ReactMarkdown children={data.text} />
			</Post>
			<CommentsBlock
				items={data.comments?.map((c) => {
					return { ...c, time: moment(c.addOrUpdateTime) }
				})}
				isLoading={false}
			>
				<Index
					value={data.comment}
					onSendBtnClick={onSendBtnClick}
					avatarUrl={authData.avatarUrl}
					onChange={(e) => {
						commonFieldUpdate('comment', e.target.value, setData)
					}}
				/>
			</CommentsBlock>
		</>
	)
}
