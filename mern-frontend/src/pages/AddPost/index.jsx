import React, { useState, useCallback, useEffect } from 'react'
import TextField from '@mui/material/TextField'
import Paper from '@mui/material/Paper'
import Button from '@mui/material/Button'
import SimpleMDE from 'react-simplemde-editor'
import { useNavigate, useParams } from 'react-router-dom'

import 'easymde/dist/easymde.min.css'
import styles from './AddPost.module.scss'
import { selectIsAuth } from '../../redux/slices/auth'
import { useSelector } from 'react-redux'
import { useRef } from 'react'
import { axios } from '../../utils/axios'
import { commonFieldUpdate } from '../../utils/field-updaters'
export const AddPost = () => {
	const isAuth = useSelector(selectIsAuth)
	const navigate = useNavigate()
	if (!window.localStorage.getItem('token') && !isAuth) return navigate('/')

	const { id } = useParams()
	const isEditing = !!id
	useEffect(() => {
		if (id) {
			const data = axios
				.get(`/posts/${id}`)
				.then((res) => {
					let { text, title, tags, imageUrl } = res.data
					tags = tags.join(', ')
					setState({ text, title, tags, imageUrl })
				})
				.catch((err) => console.warn(err))
		}
	}, [])
	const inputFileRef = useRef(null)

	const [state, setState] = useState({
		text: null,
		title: null,
		tags: null,
		imageUrl: '',
		isLoading: false,
	})

	const handleChangeFile = async (e) => {
		try {
			const formData = new FormData()
			const file = e.target.files[0]
			formData.append('image', file)
			const { data } = await axios.post('/upload', formData)
			commonFieldUpdate('imageUrl', data.url, setState)
		} catch (err) {
			console.warn(err)
		}
	}

	const onSubmit = async () => {
		try {
			commonFieldUpdate('isLoading', true, setState)
			const { isLoading, ...postData } = state
			postData.tags = postData.tags.split(',')?.map((e) => e.trim())
			const {
				data: { _id },
			} = isEditing
				? await axios.patch(`/posts/${id}`, postData)
				: await axios.post('/posts', postData)
			const __id = isEditing ? id : _id
			navigate(`/posts/${__id}`)
		} catch (err) {
			console.warn(err)
		}
	}

	const onClickRemoveImage = () => {
		commonFieldUpdate('imageUrl', null, setState)
	}

	const onChange = useCallback((value) => {
		commonFieldUpdate('text', value, setState)
	}, [])

	const options = React.useMemo(
		() => ({
			spellChecker: false,
			maxHeight: '400px',
			autofocus: true,
			placeholder: 'Введите текст...',
			status: false,
			autosave: {
				enabled: true,
				delay: 1000,
			},
		}),
		[]
	)

	return (
		<Paper style={{ padding: 30 }}>
			<Button onClick={() => inputFileRef.current.click()} variant="outlined" size="large">
				Загрузить превью
			</Button>
			<input ref={inputFileRef} type="file" onChange={handleChangeFile} hidden />
			{state.imageUrl && (
				<>
					<Button variant="contained" color="error" onClick={onClickRemoveImage}>
						Удалить
					</Button>
					<img
						className={styles.image}
						src={`http://localhost:4444${state.imageUrl}`}
						alt="Uploaded"
					/>
				</>
			)}

			<br />
			<br />
			<TextField
				value={state.title}
				classes={{ root: styles.title }}
				variant="standard"
				placeholder="Заголовок статьи..."
				onChange={(e) => {
					commonFieldUpdate('title', e.target.value, setState)
				}}
				fullWidth
			/>
			<TextField
				value={state.tags}
				onChange={(e) => {
					commonFieldUpdate('tags', e.target.value, setState)
				}}
				classes={{ root: styles.tags }}
				variant="standard"
				placeholder="Тэги"
				fullWidth
			/>
			<SimpleMDE
				className={styles.editor}
				value={state.text}
				onChange={onChange}
				options={options}
			/>
			<div className={styles.buttons}>
				<Button onClick={onSubmit} size="large" variant="contained">
					{isEditing ? 'Сохранить' : 'Опубликовать'}
				</Button>
				<a href="/">
					<Button size="large">Отмена</Button>
				</a>
			</div>
		</Paper>
	)
}
