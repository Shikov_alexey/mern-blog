import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Grid from '@mui/material/Grid'

import { Post } from '../components/Post'
import { TagsBlock } from '../components/TagsBlock'
import { CommentsBlock } from '../components/CommentsBlock'
import { fetchPosts, fetchTags } from '../redux/slices/posts'
import moment from 'moment'

export const Home = () => {
	const dispatch = useDispatch()
	const { posts, tags } = useSelector((state) => state.posts)
	const userData = useSelector((state) => state.auth.data)

	const lastFiveUniqComs = posts.items
		?.flatMap((p) => p.comments)
		.filter((e) => e)
		?.slice(0, 5)

	const isPostsLoading = posts.status === 'loading'
	const isTagsLoading = tags.status === 'loading'

	useEffect(() => {
		dispatch(fetchPosts())
		dispatch(fetchTags())
	}, [dispatch])
	return (
		<>
			<Tabs style={{ marginBottom: 15 }} value={0} aria-label="basic tabs example">
				<Tab label="Новые" />
				<Tab label="Популярные" />
			</Tabs>
			<Grid container spacing={4}>
				<Grid xs={8} item>
					{(isPostsLoading ? [...Array(5)] : posts.items).map((e, idx) =>
						isPostsLoading ? (
							<Post isLoading={isPostsLoading} key={idx} />
						) : (
							<Post
								id={e._id}
								key={e._id}
								title={e.title}
								imageUrl={e.imageUrl ? `http://localhost:4444${e.imageUrl}` : ''}
								user={e.user}
								createdAt={e.createdAt}
								viewsCount={e.viewCount}
								commentsCount={e.comments?.length}
								tags={e.tags}
								isEditable={userData?._id === e.user._id}
								isLoading={isPostsLoading}
							/>
						)
					)}
				</Grid>
				<Grid xs={4} item>
					<TagsBlock items={tags.items} isLoading={isTagsLoading} />
					<CommentsBlock
						items={lastFiveUniqComs?.map((c) => {
							return {
								...c,
								time: moment(c.addOrUpdateTime).format('HH:MM'),
							}
						})}
						isLoading={false}
					/>
				</Grid>
			</Grid>
		</>
	)
}
